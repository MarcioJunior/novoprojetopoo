package br.ucsal.bes20182.poo;

public class Onibus extends Veiculo {
	private int qtdPass;

	public Onibus(String placa, String modelo, int anoFab, double valor, int qtdPass) {
		super(placa, modelo, anoFab, valor);
		this.qtdPass = qtdPass;
	}

	public int getQtdPass() {
		return qtdPass;
	}

	public void setQtdPass(int qtdPass) {
		this.qtdPass = qtdPass;
	}
	
	public Integer locacao(int qtdPass){
		Integer custo = 0;
		custo = qtdPass * 10;
		return custo;
	}

	@Override
	public String toString() {
		return "Onibus [qtdPass=" + qtdPass + super.toString() +"]";
	}

}
