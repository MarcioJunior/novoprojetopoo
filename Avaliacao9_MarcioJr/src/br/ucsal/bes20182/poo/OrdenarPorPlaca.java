package br.ucsal.bes20182.poo;

import java.util.Comparator;

public class OrdenarPorPlaca implements Comparator<Veiculo> {

	@Override
	public int compare(Veiculo veiculo1, Veiculo veiculo2) {

		return veiculo1.getPlaca().compareToIgnoreCase(veiculo2.getPlaca());
	}

}
