package br.ucsal.bes20182.poo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Locadora {
	private static List<Veiculo> veiculos = new ArrayList<>();

	public static void cadastrarOnibus(Onibus onibus) {
		try {
			Veiculo.valorException(onibus.getValor());
			veiculos.add(onibus);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void cadastrarCaminhao(Caminhao caminhao) {
		try {
			Veiculo.valorException(caminhao.getValor());
			veiculos.add(caminhao);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void listarPorValor() {
		Collections.sort(veiculos, new OrdenarPorValor());
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo + " ");
		}
	}

	public static void listarPorPlaca() {
		Collections.sort(veiculos, new OrdenarPorPlaca());
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo + " ");
		}
	}

	public static void modelos() {
		Set<String> modelos = new HashSet<>();
		for (Veiculo veiculo : veiculos) {
			modelos.add(veiculo.getModelo());

		}
		System.out.println("Modelos" + modelos + " ");
	}

}
