package br.ucsal.bes20182.poo;

public class Caminhao extends Veiculo {
	private int eixos;
	private int carga;

	public Caminhao(String placa, String modelo, int anoFab, double valor, int eixos, int carga) {
		super(placa, modelo, anoFab, valor);
		this.eixos = eixos;
		this.carga = carga;
	}

	public int getEixos() {
		return eixos;
	}

	public void setEixos(int eixos) {
		this.eixos = eixos;
	}

	public int getCarga() {
		return carga;
	}

	public void setCarga(int carga) {
		this.carga = carga;
	}
	public Integer locacao(int carga){
		Integer custo = 0;
		custo = carga * 8;
		return custo;
	}


	@Override
	public String toString() {
		return "Caminhao [eixos=" + eixos + ", carga=" + carga +super.toString()+ "]";
	}

}
